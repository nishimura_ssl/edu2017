// Milkcocoaにコマンドを送信するスクリプト
var MilkCocoa = require('milkcocoa');
var config = require('../educonfig');

var milkcocoa = new MilkCocoa(config.mlkappid+'.mlkcca.com');
var ds = milkcocoa.dataStore(config.mlkdatastore);


var command = 'xx';
var value1 = 0;
var value2 = 0;

if( process.argv.length < 2+3 ) {
	return;
}

command = process.argv[2];
value1 = parseInt(process.argv[3], 10);
value2 = parseInt(process.argv[4], 10);

ds.push({cmd : command, val1 : value1, val2 : value2});

console.log("send {cmd:"+command + ", val1:"+value1 +", val2:"+value2 +"}");

setTimeout(function() {
	process.exit();
}, 1000);
