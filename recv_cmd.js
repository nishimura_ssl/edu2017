// Milkcocoaからの届いたコマンドに従って、顔認識処理と対話処理を実行するnode.jsスクリプト
var MilkCocoa = require('milkcocoa');
var config = require('../educonfig');

// console.log(config.mlkappid);
var milkcocoa = new MilkCocoa(config.mlkappid+'.mlkcca.com');
var ds = milkcocoa.dataStore(config.mlkdatastore);

// シェルコマンド実行用オブジェクト
const exec = require('child_process').exec;
// ファイル操作
var fs = require('fs');
// CSV/TSVデータ
var csvParse = require('csv-parse');


// titleが'raspberrypi'、contentが'start'というデータを送信＆保存
ds.push({node : 'raspberrypi', status : 'run'});

// ds.on('send', function(data) {
//  console.log('sendイベントが発生しました: ' + data.value);
// }
ds.on('push', function(data) {
	// milkcocoaから受け取ったデータを表示する
	console.log('Recieved push data -> {cmd: ' + data.value.cmd + ', val1: ' + data.value.val1 + ', val2: ' + data.value.val2 + '}' );

	if(data.value.cmd == 'fd' && data.value.val2 == 0) {
		command_fd(data.value);
	}
	// 対話処理を行う
	else if(data.value.cmd == 'tk' && data.value.val2 == 0) {
		command_tk(data.value);
	}
	// その他のコマンドを処理する
	else {
		console.log(data.value.cmd+'コマンドのイベント処理を検索します');
		command_event(data.value);
	}
});

console.log("recv_cmd.js is running on node.js.");


// fdコマンド処理
function command_fd(in_value) {
	console.log('顔認識を開始します');
	ds.push({cmd:'fd', val1:1, val2:2});

	var face_detect_file = '/home/pi/detect_face.jpg'

	if(fs.existsSync(face_detect_file)) {
		fs.unlinkSync(face_detect_file);
	}

	// Pythonを呼び出して顔認識を実行し、結果を返す。
	exec('python /home/pi/edu2017/face_detect.py', function(err, stdout, stderr) {
		var ret = 0;
		if (err) {
			console.log(err);
			ret = 0;
			ds.push({cmd:'fd', val1:ret, val2:1});
		}
		else {
			setTimeout(function() {
				if(false == fs.existsSync(face_detect_file)) {
					console.log('顔認識の結果がありません');
					ret = 0;
				}
				else {
					console.log('顔を認識しました');
					ret = 1;
				}
				ds.push({cmd:'fd', val1:ret, val2:1});
			}, 1000);
		}
		console.log(stdout);
	});
}

// tkコマンド処理
function command_tk(in_value) {
	console.log('対話処理を開始します');
	ds.push({cmd:'tk', val1:1, val2:2});
	// Pythonを呼び出して対話処理を実行する。
	exec('python /home/pi/edu2017/dialog_exec.py /home/pi/command_list.tsv', function(err, stdout, stderr) {
		var ret = 1;
		if (err) {
			console.log(err);
			ret = 0;
		}
		ds.push({cmd:'tk', val1:ret, val2:1});
		console.log(stdout);
	});
}


function command_event(in_value) {
	var event_list_file = 'home/pi/event_list.tsv';

	var rs = null;
	try {
	    rs = fs.createReadStream(event_list_file, 'utf-8');
	    rs.on('error', function (err) {
	        console.error(err);
	        process.exit(1);
	    });
	}
	catch (err) {
	    console.error(err);
	    process.exit(1);
	}

	var parser = csvParse({ delimiter: '\t' });
	parser.on('data', function (data) {
		// console.log(data);
		// console.log(data[0]);
		var event = JSON.parse(data[0]);
		// console.log(event.cmd + ' ' + event.val1 + ' ' + event.val2);

		if( in_value.cmd == event.cmd && in_value.val1 == event.val1 && in_value.val2 == event.val2 ) {
			console.log(data);
			// シェルコマンドを実行する
			console.log("シェルコマンド " + data[1] + " を実行します");
			exec(data[1], function(err, stdout, stderr) {
				if (err) {
					console.log(err);
				}
				console.log(stdout);
			});
		}
	});
	parser.on('error', function (err) {
		console.error(err);
		// process.exit(1);
	});

	rs.pipe(parser);
}
