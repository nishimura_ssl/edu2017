#!/bin/bash
HV=/usr/share/hts-voice/mei/mei_happy.htsvoice

tempfile=`tempfile`
option="-m $HV \
  -s 16000 \
  -p 100 \
  -a 0.03 \
  -u 0.0 \
  -jm 1.0 \
  -jf 1.0 \
  -x /var/lib/mecab/dic/open-jtalk/naist-jdic \
  -ow $tempfile"

if [ -z "$1" ] ; then
  /usr/bin/open_jtalk $option
else
  if [ -f "$1" ] ; then
    /usr/bin/open_jtalk $option $1
  else
    echo "$1" | /usr/bin/open_jtalk $option
  fi
fi

/usr/bin/aplay -q $tempfile
rm $tempfile
