#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Raspberry Piのカメラを使って顔認識処理を実行する

import cv2
import time

import commands


imgfile = "/home/pi/capture_face.jpg"
dtcfile = "/home/pi/detect_face.jpg"

faceCascade = cv2.CascadeClassifier('/home/pi/edu2017/haarcascade_frontalface_alt.xml')

# capture = cv2.VideoCapture(0) # カメラセット
# # 画像サイズの指定
# ret = capture.set(3, 480)
# ret = capture.set(4, 320)

def capture_image(filename):
    if filename is None:
        filename = "capture_face.jpg"
    ret = commands.getoutput("/usr/bin/raspistill -w 320 -h 240 -o " + filename)
    return ret


start = time.clock() # 開始時刻

ret = capture_image(imgfile)
image = cv2.imread(imgfile, cv2.IMREAD_COLOR)

# ret, image = capture.read() # 画像を取得する作業
# print ("capture.read() return: " + str(ret))

gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
face = faceCascade.detectMultiScale(gray_image, scaleFactor=1.3, minNeighbors=2, minSize=(30, 30))
print "number of faces = " + str(len(face))

if 0 < len(face) :
    for rect in face:
        cv2.rectangle(image, tuple(rect[0:2]), tuple(rect[0:2]+rect[2:4]), (0, 0,255), thickness=2)

    get_image_time = int((time.clock()-start)*1000) # 処理時間計測
    # 1フレーム取得するのにかかった時間を表示
    cv2.putText( image, str(get_image_time)+"ms", (10,10), 1, 1, (0,255,0))
    print("Save Image...  "+dtcfile)
    cv2.imwrite(dtcfile, image)
