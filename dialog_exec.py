# -*- coding: utf-8 -*-
#マイク0番からの音声入力を受ける。
# 一定時間(RECROD_SECONDS)だけ録音し、ファイル名：/var/tmp/mictmp.wavで保存した音声ファイルを再生する。

import pyaudio
import sys
import time
import wave
import requests
import os
import json

import csv
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
import educonfig



def recognize():
    url = "https://api.apigw.smt.docomo.ne.jp/amiVoice/v1/recognize?APIKEY={}".format(educonfig.DOCOMO_APIKEY)
    files = {"a": open(PATH, 'rb'), "v":"on"}
    r = requests.post(url, files=files)
    print r
    message = r.json()['text']
    return message

def read_listfile(message="こんにちは"):
    tsv = csv.reader(file(CMDLIST_PATH), delimiter = '\t')
    response = "何をしたら良いかな？"
    command = ""
    for row in tsv:
        # print ", ".join(row)
        if row[0] == message :
            print row[0] + " " + row[1] + " " + row[2]
            response = row[1]
            command = row[2]
            break
    return response, command

def execute_command(command="echo noexec."):
    cmdexec = command
    # print "=>" + cmdexec
    os.system(cmdexec)
    return True

def talk(message="こんにちは", card=1, device=0):
    os.system('/home/pi/edu2017/jtalk.sh "' + message + ' "')

if __name__ == '__main__':
    chunk = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    PATH = '/var/tmp/mictmp.wav'
    CMDLIST_PATH = '/home/pi/command_list.tsv'
    # あなたが取得したDocomoAPI Keyに書き換える
    # DOCOMO_APIKEY='xxxxxxxxxxxxx' #DocomoAPI Key
    CARD = 1 #OUTPUTの指定
    DEVICE = 0 #OUTPUTの指定

    #サンプリングレート、マイク性能に依存
    RATE = 16000
    #録音時間
    # RECORD_SECONDS = input('Please input recoding time>>>')
    RECORD_SECONDS = 3

    #pyaudio
    p = pyaudio.PyAudio()

    #マイク0番を設定
    input_device_index = 0
    #マイクからデータ取得
    stream = p.open(format = FORMAT,
                    channels = CHANNELS,
                    rate = RATE,
                    input = True,
                    frames_per_buffer = chunk)
    all = []
    for i in range(0, RATE / chunk * RECORD_SECONDS):
        data = stream.read(chunk)
        all.append(data)

    stream.close()
    data = ''.join(all)
    out = wave.open(PATH,'w')
    out.setnchannels(1) #mono
    out.setsampwidth(2) #16bits
    out.setframerate(RATE)
    out.writeframes(data)
    out.close()

    p.terminate()

    message = recognize()
    message = message.encode("utf-8")
    # message = "前進。"
    # if message != "。" :
    if message != "" :
        print "### 認識された音声 => " + message
        talk_message, command = read_listfile(message)
        talk(talk_message, CARD, DEVICE)
        execute_command(command)
